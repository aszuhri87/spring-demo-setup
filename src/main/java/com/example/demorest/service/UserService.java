package com.example.demorest.service;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.demorest.model.User;
import com.example.demorest.repository.UserRepo;

@Repository
public class UserService implements UserRepo {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Override
  public int save(User user) {
    return jdbcTemplate.update("INSERT INTO users (name, username, password) VALUES(?,?,?)",
        new Object[] { user.getName(), user.getUsername(), user.getPassword()});
  }

  @Override
  public int update(User user) {
    return jdbcTemplate.update("UPDATE users SET name=?, username=?, password=? WHERE id=?",
        new Object[] { user.getName(), user.getUsername(), user.getPassword(), user.getId() });
  }

  @Override
  public User findById(UUID id) {
    try {
      User user = jdbcTemplate.queryForObject("SELECT * FROM users WHERE id=?",
          BeanPropertyRowMapper.newInstance(User.class), id);

      return user;
    } catch (IncorrectResultSizeDataAccessException e) {
      return null;
    }
  }

  @Override
  public int deleteById(UUID id) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'deleteById'");
  }

  @Override
  public List<User> findAll() {
    return jdbcTemplate.query("SELECT * from users", BeanPropertyRowMapper.newInstance(User.class));
  }

  @Override
  public List<User> findByNameContaining(String name) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'findByNameContaining'");
  }

  @Override
  public int deleteAll() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'deleteAll'");
  }
}