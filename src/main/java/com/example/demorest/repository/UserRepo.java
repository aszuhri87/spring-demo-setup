package com.example.demorest.repository;

import java.util.List;
import java.util.UUID;

import com.example.demorest.model.User;

public interface UserRepo {
  int save(User users);

  int update(User users);

  User findById(UUID id);

  int deleteById(UUID id);

  List<User> findAll();

  List<User> findByNameContaining(String name);

  int deleteAll();
}
