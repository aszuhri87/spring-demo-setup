package com.example.demorest.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.UUID;

public class User {
  private UUID id;
  private String name;
  private String username;
  private String password;
  private Timestamp created_at;
  private Timestamp updated_at;
  private Timestamp deleted_at;

  public User() {

  }
  
  public User(UUID id, String name, String username, String password, Timestamp created_at, Timestamp updated_at, Timestamp deleted_at) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.password = password;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.deleted_at = deleted_at;
  }

  public User(String name, String username, String password) {
    this.name = name;
    this.username = username;
    this.password = password;
  }
  
  public void setId(UUID id) {
    this.id = id;
  }
  
  public UUID getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

    public String getPassword() {
    return password;
  }

  public void setPassward(String password) {
    this.password = password;
  }


  @Override
  public String toString() {
    return "user [id=" + id + ", name=" + name + ", username=" + username + "]";
  }


}
